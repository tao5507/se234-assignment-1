package corejava.day10;

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class Enemy {
    public final static int LEFT_DOWN_1 = 0;
    public final static int LEFT_DOWN_2 = 1;
    public final static int LEFT_DOWN_3 = 2;
    public final static int RIGHT_DOWN_1 = 3;
    public final static int RIGHT_DOWN_2 = 4;
    public final static int RIGHT_DOWN_3 = 5;
    public final static int DOWN = 6;

    private int x, y;//敌机中心坐标
    private int orientation;
    private boolean isBombed;
    private JPanel panel;
    private static Image enemyPlane = new ImageIcon("pic/enemy.gif").getImage();
    private static Image enemyPlane_L = new ImageIcon("pic/enemy_l.gif").getImage();
    private static Image enemyPlane_R = new ImageIcon("pic/enemy_r.gif").getImage();

    public Enemy(int x, int y, int orientation, JPanel panel) {
        this.x = x;
        this.y = y;
        this.orientation = orientation;
        this.panel = panel;

    }
    public Enemy(){}
    public void draw(Graphics g) {
        Image tempEnemy;
        if(orientation<3)
            tempEnemy=enemyPlane_L;
        else if(3<=orientation&&orientation<6)
            tempEnemy=enemyPlane_R;
        else
            tempEnemy=enemyPlane;
        g.drawImage(tempEnemy, x-40, y-40, 80,80, panel);
    }
}
