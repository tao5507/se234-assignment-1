package corejava.day10;
import javax.swing.JFrame;

public class FighterFrame {
	private JFrame frame;

	public FighterFrame(){
		frame=new JFrame("Mario");

	}
	public void show(){
		frame.setBounds(300, 10, 600,600);
		frame.setResizable(false);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	public static void main(String[] args) {
		new FighterFrame().show();
	}
}